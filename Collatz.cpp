// -----------
// Collatz.c++
// -----------

// --------
// includes
// --------

#include <cassert>  // assert
#include <cmath>    // max, min
#include <iostream> // endl, istream, ostream
#include <sstream>  // istringstream
#include <string>   // getline, string
#include <tuple>    // make_tuple, tie, tuple
#include <utility>  // make_pair, pair

#include "Collatz.hpp"

using namespace std;

const int SIZE = 500000;

int memo_table[SIZE];

// ------------
// collatz_read
// ------------

pair<int, int> collatz_read (const string& s) {
    istringstream sin(s);
    int i, j;
    sin >> i >> j;
    return make_pair(i, j);
}

// ----------------
// write_memo_table
// ----------------

void write_memo_table (long index, int cycle) {
    assert(index > 0);
    assert(cycle > 0);
    if (index <= SIZE) {
        assert(memo_table[index - 1] == 0);
        memo_table[index - 1] = cycle;
    }
}

// ---------------
// read_memo_table
// ---------------

int read_memo_table (long index) {
    assert(index > 0);
    if (index > SIZE) {
        return 0;
    }
    return memo_table[index - 1];
}

// ----------
// calc_cycle
// ----------

int calc_cycle (long value) {
    assert(value > 0);
    int cycle = read_memo_table(value);
    if (cycle == 0) {
        if (value == 1) {
            cycle = 1;
        } else if (value % 2 == 0) {
            cycle = 1 + calc_cycle(value / 2);
        } else {
            cycle = 2 + calc_cycle(value + (value >> 1) + 1);
        }
        write_memo_table(value, cycle);
    }
    assert(cycle > 0);
    return cycle;
}

// ---------
// max_cycle
// ---------

int max_cycle (int mini, int maxi) {
    assert(mini <= maxi);
    int result = 0;
    int opt_mini = max(mini, (maxi / 2) + 1);
    for (int i = opt_mini; i <= maxi; ++i) {
        int cycle = calc_cycle(i);
        if (cycle > result) {
            result = cycle;
        }
    }
    assert(result > 0);
    return result;
}

// ------------
// collatz_eval
// ------------

tuple<int, int, int> collatz_eval (const pair<int, int>& p) {
    int i, j;
    tie(i, j) = p;
    int mini = min(i, j);
    int maxi = max(i, j);
    int result = max_cycle(mini, maxi);
    return make_tuple(i, j, result);
}

// -------------
// collatz_print
// -------------

void collatz_print (ostream& sout, const tuple<int, int, int>& t) {
    int i, j, v;
    tie(i, j, v) = t;
    sout << i << " " << j << " " << v << endl;
}

// -------------
// collatz_solve
// -------------

void collatz_solve (istream& sin, ostream& sout) {
    string s;
    while (getline(sin, s))
        collatz_print(sout, collatz_eval(collatz_read(s)));
}
