// ---------
// Collatz.h
// ---------

#ifndef Collatz_h
#define Collatz_h

// --------
// includes
// --------

#include <iostream> // istream, ostream
#include <tuple>    // tuple
#include <utility>  // pair

using namespace std;

// ------------
// collatz_read
// ------------

/**
 * read two ints
 * @param a string
 * @return a pair of ints
 */
pair<int, int> collatz_read (const string&);

// ----------------
// write_memo_table
// ----------------

/**
 * @param index to write to
 * @param value to write
 */
void write_memo_table (long, int);

// ---------------
// read_memo_table
// ---------------

/**
 * @param index to read from
 */
int read_memo_table (long);

// ----------
// calc_cycle
// ----------

/**
 * calculates the cycle length of a given value
 * @param value to calculate cycle length of
 */
int calc_cycle (long);

// ---------
// max_cycle
// ---------

/**
 * calculates the maximum cycle length of a given range
 * @param minimum value
 * @param maximum value
 */
int max_cycle (int, int);

// ------------
// collatz_eval
// ------------

/**
 * @param a pair of ints
 * @return a tuple of three ints
 */
tuple<int, int, int> collatz_eval (const pair<int, int>&);

// -------------
// collatz_print
// -------------

/**
 * print three ints
 * @param an ostream
 * @param a tuple of three ints
 */
void collatz_print (ostream&, const tuple<int, int, int>&);

// -------------
// collatz_solve
// -------------

/**
 * @param an istream
 * @param an ostream
 */
void collatz_solve (istream&, ostream&);

#endif // Collatz_h
