# CS371p: Object-Oriented Programming Collatz Repo

* Name: Bren Keeton

* EID: tck548

* GitLab ID: tkeeton

* HackerRank ID: keeton

* Git SHA: 2900174b3d1d150cc62bf4d5bab11ae728d3182d

* GitLab Pipelines: gitlab.com/tkeeton/cs371p-collatz/-/pipelines

* Estimated completion time: 6 hours

* Actual completion time: ~4-4.5 hours

* Comments: (any additional comments you have)
